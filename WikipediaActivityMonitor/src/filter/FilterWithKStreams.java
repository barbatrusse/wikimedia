package filter;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * 
 * @author V. Galtier
 * 
 *         Read Wikimedia change events from a Kafka topic, filter out the ones
 *         that concern the Wikipedia project, and publish for each of these the
 *         locale of the project to a different Kafka topic.
 */
public class FilterWithKStreams {

	/*
	 * List of Kafka bootstrap servers. Example: localhost:9092,another.host:9092
	 * 
	 * @see:
	 * https://jaceklaskowski.gitbooks.io/apache-kafka/content/kafka-properties-
	 * bootstrap-servers.html
	 */
	private String bootstrapServers;
	/*
	 * Name of the source Kafka topic
	 */
	private String wikimediaChangesTopicName;
	/*
	 * Name of the destination Kafka topic
	 */
	private String localeTopicName;

	public static void main(String[] arg) {
		new FilterWithKStreams(arg[0], arg[1], arg[2]);
	}

	FilterWithKStreams(String bootstrapServers, String wikimediaChangesTopicName, String localeTopicName) {
		this.bootstrapServers = bootstrapServers;
		this.wikimediaChangesTopicName = wikimediaChangesTopicName;
		this.localeTopicName = localeTopicName;

		Topology wikipediaLocaleTopology = createWikipediaLocaleTopology();
		KafkaStreams wikipediaLocaleStream = new KafkaStreams(wikipediaLocaleTopology,
				configureWikipediaLocaleKafkaStreams());
		wikipediaLocaleStream.start();
	}

	private Properties configureWikipediaLocaleKafkaStreams() {
		Properties properties = new Properties();
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "wikipediaLocale");
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		// The semantics of caching is that data is flushed to the state store and
		// forwarded to the next downstream processor node
		// whenever the earliest of
		// commit.interval.ms or cache.max.bytes.buffering (cache pressure) hits.
		properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
		properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Void().getClass().getName());
		properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		return properties;
	}

	private Topology createWikipediaLocaleTopology() {
		StreamsBuilder streamsBuilder = new StreamsBuilder();

		KStream<Void, String> wikimediaChangesStream = streamsBuilder.stream(wikimediaChangesTopicName);

		// Filter out Wikipedia events
		// ----------------------------
		Predicate<Void, String> predicateWikipediaOnly = new Predicate<Void, String>() {

			@Override
			public boolean test(Void key, String value) {
				Gson gson = new Gson();
				JsonObject jsonObject = gson.fromJson(value, JsonObject.class);
				JsonObject jsonObjectMeta = jsonObject.getAsJsonObject("meta");
				String domain = jsonObjectMeta.getAsJsonPrimitive("domain").getAsString();
				return domain.contains("wikipedia");
			}
		};
		KStream wikipediaStream = wikimediaChangesStream.filter(predicateWikipediaOnly);

		// Keep only the locale part of the 'domain' field
		// ------------------------------------------------
		ValueTransformerSupplier<String, String> valueTransformerKeepOnlyLocale = new ValueTransformerSupplier<String, String>() {

			@Override
			public ValueTransformer<String, String> get() {
				return new ValueTransformer<String, String>() {

					@Override
					public void init(ProcessorContext context) {
						// TODO Auto-generated method stub
					}

					@Override
					public String transform(String value) {
						Gson gson = new Gson();
						JsonObject jsonObject = gson.fromJson(value, JsonObject.class);
						JsonObject jsonObjectMeta = jsonObject.getAsJsonObject("meta");
						String domain = jsonObjectMeta.getAsJsonPrimitive("domain").getAsString();
						// domain = en.wikipedia.org
						return domain.split("\\.")[0]; // '.' is a special character in Java regex, must be escaped
					}

					@Override
					public void close() {
						// TODO Auto-generated method stub
					}
				};
			}
		};
		KStream<Void, String> localeStream = wikipediaStream.transformValues(valueTransformerKeepOnlyLocale);

		// Output to the appropriate Kafka topic
		// --------------------------------------
		localeStream.to(localeTopicName);

		return streamsBuilder.build();
	}

}