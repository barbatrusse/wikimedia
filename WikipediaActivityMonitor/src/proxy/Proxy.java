package proxy;

import java.net.URI;
import java.util.Properties;
//import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.EventSource;
import com.launchdarkly.eventsource.MessageEvent;

/**
 * 
 * @author V. Galtier
 * 
 *         Act as a proxy to Wikimedia changes web stream for a Kafka-based
 *         application: Read the stream of Wikimedia changes and write them to a
 *         Kafka topic. The stream-reading part is derived from
 *         https://golb.hplar.ch/2018/02/Access-Server-Sent-Events-from-Java.html
 *
 */
public class Proxy implements EventHandler {

	/*
	 * Kafka producer
	 */
	private KafkaProducer<Void, String> kafkaProducer;
	/*
	 * List of Kafka bootstrap servers. Example: localhost:9092,another.host:9092
	 * 
	 * @see:
	 * https://jaceklaskowski.gitbooks.io/apache-kafka/content/kafka-properties-
	 * bootstrap-servers.html
	 */
	private String bootstrapServers;
	/*
	 * Name of the destination Kafka topic
	 */
	private String topicName;

	/**
	 * Create the proxy (provoking infinite execution).
	 * 
	 * @param args first argument is a list of Kafka bootstrap servers, second
	 *             argument is the name of the destination Kafka topic
	 */
	public static void main(String[] args) {
		new Proxy(args[0], args[1]);
	}

	/**
	 * Create a web stream listener and a Kafka producer, the producer publishes to
	 * the Kafka topic the events received from the web stream, it does until
	 * interrupted.
	 * 
	 * @param bootstrapServers list of Kafka bootstrap servers. Example:
	 *                         localhost:9092,another.host:9092
	 * @param topicName        name of the destination Kafka topic
	 */
	Proxy(String bootstrapServers, String topicName) {
		this.bootstrapServers = bootstrapServers;
		this.topicName = topicName;
		try {
			// create the Kafka producer with the appropriate configuration
			kafkaProducer = new KafkaProducer<>(configureKafkaProducer());

			// create and start the web stream listener
			// see the onMessage method for the actions taken when an event occurs
			String url = "https://stream.wikimedia.org/v2/stream/recentchange";
			EventSource.Builder builder = new EventSource.Builder(this, URI.create(url));
			EventSource eventSource = builder.build();
			eventSource.start();

			// listen to the stream for 5 seconds
			/*
			 * try { TimeUnit.SECONDS.sleep(5); } catch (InterruptedException e) {
			 * e.printStackTrace(); }
			 */
			// listen to the stream for ever
			while (true) {
				// empty on purpose
			}
		} catch (Exception e) {
			System.err.println("something went wrong... " + e.getMessage());
		} finally {
			kafkaProducer.close();
		}
	}

	@Override
	public void onOpen() throws Exception {
		System.out.println("The stream connection has been opened.");
	}

	@Override
	public void onClosed() throws Exception {
		System.out.println("The stream connection has been closed.");
	}

	/**
	 * When a message comes on the stream, write it to the Kafka topic.
	 */
	@Override
	public void onMessage(String event, MessageEvent messageEvent) throws Exception {
		String message = messageEvent.getData();
		kafkaProducer.send(new ProducerRecord<Void, String>(topicName, null, message));
	}

	@Override
	public void onComment(String comment) throws Exception {
		System.out.println("A comment line (any line starting with a colon) was received from the stream: " + comment);
	}

	@Override
	public void onError(Throwable t) {
		System.out.println("An exception occured on the socket connection: " + t.getMessage());
	}

	/**
	 * Prepare configuration for the Kafka producer <Void, String> Note: the
	 * BOOTSTRAP_SERVERS_CONFIG property is set to the first argument on the
	 * command-line
	 * 
	 * @return configuration properties for the Kafka producer
	 */
	private Properties configureKafkaProducer() {
		Properties producerProperties = new Properties();
		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.VoidSerializer");
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		return producerProperties;
	}
}